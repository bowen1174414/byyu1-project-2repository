package project2;

import bagel.Image;
import bagel.util.Point;
import bagel.util.Rectangle;
import project2.element.Element;
import project2.element.Sailor;
import project2.element.enemy.Enemy;
import project2.element.item.Elixir;
import project2.element.item.Item;
import project2.element.item.Sword;
import project2.element.projectile.Projectile;
import project2.element.stationary.Stationary;
import project2.element.stationary.Treasure;
import project2.util.Direction;
import project2.util.FontUtil;

import java.util.List;
import java.util.stream.Collectors;

public class GameMap {
    private List<Enemy> enemies;
    private List<Projectile> projectiles;
    private List<Item> items;
    private List<Stationary> stationaries;
    private Sailor sailor;

    private Image backgroundImage;
    private Point topLeftPoint;
    private Point bottomRightPoint;



    public void draw() {
        backgroundImage.drawFromTopLeft(0, 0);
        stationaries.forEach(Stationary::draw);
        items.forEach(Item::draw);
        enemies.forEach(Enemy::draw);
        projectiles.forEach(Projectile::draw);
        sailor.draw();

        drawHealthPoint();
        drawSailorItems();
    }

    private void drawHealthPoint() {
        int percentage = sailor.getHealthPoint() * 100 / sailor.getHealthPointMaximum();
        FontUtil.drawSailorHealthPoint(percentage);
    }

    private void drawSailorItems() {
        int y = 40;
        for (Item item : sailor.getItems()) {
            item.getIcon().drawFromTopLeft(10, y);
            y += 40;
        }
    }

    public void update() {
        sailor.update();

        if (sailor.isAttacking()) {
            enemies.stream().filter(e -> checkCollision(e.getPosition(), e, sailor)).forEach(e -> e.onHarmed(sailor.getDamagePoint()));
        }

        enemies = enemies.stream().filter(e -> e.getHealthPoint() > 0).collect(Collectors.toList());

        enemies.forEach(e -> {
            Point nextPosition = e.nextPosition();
            if (isInBounds(nextPosition, e) && canEnemyMove(nextPosition, e)) {
                e.move(nextPosition);
            } else {
                e.setDirection(e.getDirection() == Direction.Right ? Direction.Left : Direction.Right);
            }
            e.update();
            if (checkIfSailorInRange(e) && !e.isCoolingDown()) {
                projectiles.add(e.createProjectile(new Point(sailor.getPosition().x + sailor.getWidth() / 2, sailor.getPosition().y + sailor.getHeight() / 2)));
                e.onAttacked();
            }
        });

        stationaries.forEach(Stationary::update);
        stationaries = stationaries.stream().filter(s -> !s.shouldBeRemoved()).collect(Collectors.toList());

        projectiles.forEach(Projectile::update);
        projectiles = projectiles.stream().filter(p -> {
            if (checkCollision(p.getPosition(), p, sailor)) {
                sailor.setHealthPoint(sailor.getHealthPoint() - p.getDamagePoint());
                return false;
            }
            return isInBounds(p.getPosition(), p);
        }).collect(Collectors.toList());
    }

    private boolean canEnemyMove(Point nextPosition, Enemy enemy) {
        for (Stationary stationary : stationaries) {
            if (stationary.canMoveThrough()) {
                continue;
            }
            if (checkCollision(nextPosition, enemy, stationary)) {
                return false;
            }
        }
        return true;
    }

    public void moveSailor(Direction direction) {
        sailor.setDirection(direction);
        Point nextPosition = sailor.nextPosition();
        if (isInBounds(nextPosition, sailor)) {
            for (Stationary stationary : stationaries) {
                if (checkCollision(nextPosition, sailor, stationary)) {
                    stationary.onSailorBumped(sailor);
                    if (stationary.canMoveThrough()) {
                        sailor.move(nextPosition);
                    }
                    return;
                }
            }
            for (Item item : items) {
                if (checkCollision(nextPosition, sailor, item)) {
                    pickUpItem(item);
                    break;
                }
            }
            sailor.move(nextPosition);
        }
    }

    public void sailorAttack() {
        sailor.attack();
    }

    public void pickUpItem(Item item) {
        item.effect(sailor);
        items.remove(item);
        sailor.getItems().add(item);
    }

    public boolean checkCollision(Point nextPosition, Element e1, Element e2) {
        Rectangle r1 = new Rectangle(nextPosition, e1.getWidth(), e1.getHeight());
        Rectangle r2 = new Rectangle(e2.getPosition(), e2.getWidth(), e2.getHeight());
        return r1.intersects(r2);
    }

    public boolean isInBounds(Point position, Element element) {
        double left = position.x;
        double right = position.x;
        double top = position.y;
        double bottom = position.y;
        return left >= topLeftPoint.x && right <= bottomRightPoint.x && top >= topLeftPoint.y && bottom <= bottomRightPoint.y;
    }

    public boolean checkIfSailorOnLadder() {
        Point position = sailor.getPosition();
        return position.x >= 990 && position.y >= 630;
    }

    public boolean checkIfSailorDead() {
        return sailor.getHealthPoint() <= 0;
    }

    public boolean checkIfWin() {
        for (Stationary stationary : stationaries) {
            if (stationary instanceof Treasure && checkCollision(sailor.getPosition(), sailor, stationary)) {
                return true;
            }
        }
        return false;
    }

    public boolean checkIfSailorInRange(Enemy enemy) {
        int range = enemy.getRange();
        Point centerPosition = new Point(enemy.getPosition().x + enemy.getWidth() / 2, enemy.getPosition().y + enemy.getHeight() / 2);
        Point leftUpPosition = new Point(centerPosition.x - range / 2, centerPosition.y - range / 2);
        Rectangle rectangle = new Rectangle(leftUpPosition, range, range);
        Rectangle sailorRectangle = new Rectangle(sailor.getPosition(), sailor.getWidth(), sailor.getHeight());
        return rectangle.intersects(sailorRectangle);
    }

    public Point getTopLeftPoint() {
        return topLeftPoint;
    }

    public void setTopLeftPoint(Point topLeftPoint) {
        this.topLeftPoint = topLeftPoint;
    }

    public Point getBottomRightPoint() {
        return bottomRightPoint;
    }

    public void setBottomRightPoint(Point bottomRightPoint) {
        this.bottomRightPoint = bottomRightPoint;
    }

    public Image getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(Image backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public List<Enemy> getEnemies() {
        return enemies;
    }

    public void setEnemies(List<Enemy> enemies) {
        this.enemies = enemies;
    }

    public List<Projectile> getProjectiles() {
        return projectiles;
    }

    public void setProjectiles(List<Projectile> projectiles) {
        this.projectiles = projectiles;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<Stationary> getStationaries() {
        return stationaries;
    }

    public void setStationaries(List<Stationary> stationaries) {
        this.stationaries = stationaries;
    }

    public Sailor getSailor() {
        return sailor;
    }

    public void setSailor(Sailor sailor) {
        this.sailor = sailor;
    }
}
