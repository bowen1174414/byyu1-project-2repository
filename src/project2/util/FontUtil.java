package project2.util;

import bagel.DrawOptions;
import bagel.Font;
import bagel.util.Colour;
import bagel.util.Point;

public class FontUtil {
    static Font font;
    static Font sailorHealthPointFont;
    static Font enemyHealthPointFont;

    static {
        font = new Font("res/wheaton.otf", 55);
        sailorHealthPointFont = new Font("res/wheaton.otf", 30);
        enemyHealthPointFont = new Font("res/wheaton.otf", 15);
    }

    public static void drawString(String str, double x, double y, Colour colour) {
        DrawOptions options = new DrawOptions();
        options.setBlendColour(colour);
        font.drawString(str, x, y, options);
    }

    public static void drawSailorHealthPoint(int percentage) {
        DrawOptions options = new DrawOptions();
        options.setBlendColour(getHealthPointColour(percentage));
        sailorHealthPointFont.drawString("" + percentage + "%", 10, 25, options);
    }

    public static void drawEnemyHealthPoint(Point position, int percentage) {
        DrawOptions options = new DrawOptions();
        options.setBlendColour(getHealthPointColour(percentage));
        enemyHealthPointFont.drawString("" + percentage + "%", position.x, position.y - 6, options);
    }

    private static Colour getHealthPointColour(int percentage) {
        Colour colour = null;
        if (percentage < 65 && percentage >= 35) {
            colour = new Colour(0.9, 0.6, 0);
        } else if (percentage < 35) {
            colour = new Colour(1, 0, 0);
        } else {
            colour = new Colour(0, 0.8, 0.2);
        }
        return colour;
    }

    public static double getTextWidth(String text) {
        return font.getWidth(text);
    }
}
