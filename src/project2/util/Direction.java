package project2.util;

public enum Direction {
    Right("Right", 1, 0),
    Left("Left", -1, 0),
    Down("Down", 0, 1),
    Up("Up", 0, -1),
    None("None", 0, 0);
    public final int x, y;
    public final String key;

    Direction(String key, int x, int y) {
        this.key = key;
        this.x = x;
        this.y = y;
    }
}
