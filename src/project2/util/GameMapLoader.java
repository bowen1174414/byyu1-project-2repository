package project2.util;

import bagel.util.Point;
import project2.GameMap;
import project2.element.Sailor;
import project2.element.enemy.Blackbeard;
import project2.element.enemy.Enemy;
import project2.element.enemy.Pirate;
import project2.element.item.Elixir;
import project2.element.item.Item;
import project2.element.item.Potion;
import project2.element.item.Sword;
import project2.element.stationary.Block;
import project2.element.stationary.Bomb;
import project2.element.stationary.Stationary;
import project2.element.stationary.Treasure;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GameMapLoader {
    public static GameMap createGameMap(String fileName, int level) {
        GameMap gameMap = new GameMap();
        List<Enemy> enemies = new ArrayList<>();
        List<Item> items = new ArrayList<>();
        List<Stationary> stationaries = new ArrayList<>();
        Sailor sailor = null;
        try {
            Scanner scanner = new Scanner(new File(fileName));
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] parts = line.split(",");
                int x = Integer.parseInt(parts[1]);
                int y = Integer.parseInt(parts[2]);
                Point location = new Point(x, y);
                String key = parts[0];

                switch (key) {
                    case "Sailor": sailor = new Sailor(location); break;
                    case "Pirate": enemies.add(new Pirate(location)); break;
                    case "Blackbeard": enemies.add(new Blackbeard(location)); break;
                    case "Block": stationaries.add(level == 0 ? new Block(location) : new Bomb(location)); break;
                    case "Bomb": stationaries.add(new Bomb(location)); break;
                    case "Treasure": stationaries.add(new Treasure(location)); break;
                    case "Elixir": items.add(new Elixir(location)); break;
                    case "Potion": items.add(new Potion(location)); break;
                    case "Sword": items.add(new Sword(location)); break;
                    case "BottomRight": gameMap.setBottomRightPoint(location); break;
                    case "TopLeft": gameMap.setTopLeftPoint(location); break;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        gameMap.setEnemies(enemies);
        gameMap.setItems(items);
        gameMap.setProjectiles(new ArrayList<>());
        gameMap.setSailor(sailor);
        gameMap.setStationaries(stationaries);
        gameMap.setBackgroundImage(Images.backgroundImage(level));

        return gameMap;
    }
}
