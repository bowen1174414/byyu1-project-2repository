package project2.util;

import bagel.Image;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Images {
    public static final String BLACKBEARD = "blackbeard/blackbeard";
    public static final String BLACKBEARD_HIT = "blackbeard/blackbeardHit";
    public static final String PIRATE = "pirate/pirate";
    public static final String PIRATE_HIT = "pirate/pirateHit";
    public static final String SAILOR = "sailor/sailor";
    public static final String SAILOR_HIT = "sailor/sailorHit";

    private static final Map<String, Map<Direction, Image>> movingImages;

    private static final Image backgroundImage0 = new Image("res/background0.png");
    private static final Image backgroundImage1 = new Image("res/background1.png");

    static {
        String[] names = {
                "pirate/pirate",
                "pirate/pirateHit",
                "blackbeard/blackbeard",
                "blackbeard/blackbeardHit",
                "sailor/sailor",
                "sailor/sailorHit"
        };
        Direction[] directions = { Direction.Left, Direction.Right };
        movingImages = new HashMap<>();
        Arrays.stream(names).forEach(name -> {
            Map<Direction, Image> images = new HashMap<>();
            Arrays.stream(directions).forEach(d -> images.put(d, new Image("res/" + name + d.key + ".png")));
            movingImages.put(name, images);
        });
    }

    public static Image getImage(String key, Direction direction) {
        return movingImages.get(key).get(direction);
    }

    public static Image backgroundImage(int level) {
        return level == 0 ? backgroundImage0 : level == 1 ? backgroundImage1 : null;
    }

    public static final Image ElixirImage = new Image("res/items/elixir.png");
    public static final Image ElixirIcon = new Image("res/items/elixirIcon.png");

    public static final Image PotionImage = new Image("res/items/potion.png");
    public static final Image PotionIcon = new Image("res/items/potionIcon.png");

    public static final Image SwordImage = new Image("res/items/sword.png");
    public static final Image SwordIcon = new Image("res/items/swordIcon.png");

    public static final Image BlockImage = new Image("res/block.png");
    public static final Image BombImage = new Image("res/bomb.png");
    public static final Image ExplosionImage = new Image("res/explosion.png");
    public static final Image TreasureImage = new Image("res/treasure.png");

    public static final Image BlackbeardProjectile = new Image("res/blackbeard/blackbeardProjectile.png");
    public static final Image PirateProjectile = new Image("res/pirate/pirateProjectile.png");

}
