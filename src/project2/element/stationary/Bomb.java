package project2.element.stationary;

import bagel.util.Point;
import project2.element.Sailor;
import project2.util.Images;

public class Bomb extends Stationary {
    private boolean hasExploded;
    private int timeOut = 30;
    public Bomb(Point position) {
        super(position);
        width = 32;
        height = 40;
    }

    @Override
    public void draw() {
        if (timeOut == 0) {
        } else if (!hasExploded) {
            Images.BombImage.drawFromTopLeft(position.x, position.y);
        } else {
            Images.ExplosionImage.drawFromTopLeft(position.x, position.y);
        }
    }

    public void explode() {
        hasExploded = true;
    }

    @Override
    public void update() {
        if (hasExploded && timeOut != 0) {
            timeOut--;
        }
    }

    @Override
    public boolean canMoveThrough() {
        return timeOut == 0;
    }

    @Override
    public void onSailorBumped(Sailor sailor) {
        if (!hasExploded) {
            sailor.setHealthPoint(sailor.getHealthPoint() - 10);
            explode();
        }
    }

    @Override
    public boolean shouldBeRemoved() {
        return hasExploded && timeOut == 0;
    }
}
