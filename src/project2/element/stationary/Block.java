package project2.element.stationary;

import bagel.util.Point;
import project2.util.Images;

public class Block extends Stationary {
    public Block(Point position) {
        super(position);
        width = 32;
        height = 40;
    }

    @Override
    public void draw() {
        Images.BlockImage.drawFromTopLeft(position.x, position.y);
    }

    @Override
    public boolean canMoveThrough() {
        return false;
    }
}
