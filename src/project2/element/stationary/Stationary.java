package project2.element.stationary;

import bagel.util.Point;
import project2.element.Element;
import project2.element.Sailor;
import project2.element.Updatable;

public class Stationary extends Element implements Updatable {
    public Stationary(Point position) {
        super(position);
    }

    @Override
    public void update() {}

    public void onSailorBumped(Sailor sailor) {}

    public boolean canMoveThrough() {
        return true;
    }

    public boolean shouldBeRemoved() {
        return false;
    }
}
