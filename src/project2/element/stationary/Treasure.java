package project2.element.stationary;

import bagel.util.Point;
import project2.util.Images;

public class Treasure extends Stationary {
    public Treasure(Point position) {
        super(position);
        width = 40;
        height = 48;
    }

    @Override
    public void draw() {
        Images.TreasureImage.drawFromTopLeft(position.x, position.y);
    }

}
