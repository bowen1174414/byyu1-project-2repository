package project2.element;

import bagel.util.Point;
import project2.util.Direction;

public abstract class MovingElement extends Element implements Updatable {
    protected String key = "";
    protected Direction facingDirection = Direction.Right;
    protected Direction direction = Direction.None;
    public abstract double getSpeed();

    public MovingElement(Point position) {
        super(position);
    }

    public MovingElement() {
    }

    public void move(Point nextPosition) {
        position = nextPosition;
    }

    public Point nextPosition() {
        double speed = getSpeed();
        return new Point(position.x + direction.x * speed, position.y + direction.y * speed);
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
        if (direction == Direction.Right || direction == Direction.Left) {
            facingDirection = direction;
        }
    }

    public Direction getDirection() {
        return direction;
    }
}
