package project2.element;

import bagel.util.Point;

public abstract class Element {
    protected Point position;
    protected int width;
    protected int height;

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void draw() {}

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    public Element(Point position) {
        this.position = position;
    }

    public Element() {
    }
}
