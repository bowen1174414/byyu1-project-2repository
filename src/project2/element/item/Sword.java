package project2.element.item;

import bagel.Image;
import bagel.util.Point;
import project2.element.Sailor;
import project2.util.Images;

public class Sword extends Item {
    public Sword(Point position) {
        super(position);
    }

    @Override
    public void effect(Sailor sailor) {
        sailor.setDamagePoint(sailor.getDamagePoint() + 15);
    }

    @Override
    public void draw() {
        Images.SwordImage.drawFromTopLeft(position.x, position.y);
    }

    @Override
    public Image getIcon() {
        return Images.SwordIcon;
    }
}
