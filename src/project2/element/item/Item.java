package project2.element.item;

import bagel.Image;
import bagel.util.Point;
import project2.element.Element;
import project2.element.Sailor;

public abstract class Item extends Element {
    public Item(Point position) {
        this.position = position;
    }

    public abstract void effect(Sailor sailor);

    public abstract Image getIcon();
}
