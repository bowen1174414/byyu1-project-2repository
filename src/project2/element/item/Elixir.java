package project2.element.item;

import bagel.Image;
import bagel.util.Point;
import project2.element.Sailor;
import project2.util.Images;

public class Elixir extends Item {
    public Elixir(Point position) {
        super(position);
    }

    @Override
    public void effect(Sailor sailor) {
        sailor.setHealthPointMaximum(sailor.getHealthPointMaximum() + 35);
        sailor.setHealthPoint(sailor.getHealthPointMaximum());
    }

    @Override
    public void draw() {
        Images.ElixirImage.drawFromTopLeft(position.x, position.y);
    }

    @Override
    public Image getIcon() {
        return Images.ElixirIcon;
    }
}
