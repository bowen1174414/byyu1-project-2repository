package project2.element.item;

import bagel.Image;
import bagel.util.Point;
import project2.element.Sailor;
import project2.util.Images;

public class Potion extends Item {
    public Potion(Point position) {
        super(position);
    }

    @Override
    public void effect(Sailor sailor) {
        sailor.setHealthPoint(Math.min(sailor.getHealthPoint() + 25, sailor.getHealthPointMaximum()));
    }

    @Override
    public void draw() {
        Images.PotionImage.drawFromTopLeft(position.x, position.y);
    }

    @Override
    public Image getIcon() {
        return Images.PotionIcon;
    }
}
