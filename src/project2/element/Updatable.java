package project2.element;

public interface Updatable {
    void update();
}
