package project2.element;

import bagel.Image;
import bagel.util.Point;
import project2.element.item.Item;
import project2.util.Images;

import java.util.ArrayList;
import java.util.List;

public class Sailor extends MovingElement {
    private int healthPoint = 100;
    private int healthPointMaximum = 100;
    private int damagePoint = 15;
    private List<Item> items = new ArrayList<>();
    @Override
    public void update() {
        if (state == State.Attack) {
            timeOut--;
            if (timeOut == 0) {
                state = State.CoolDown;
                timeOut = COOL_DOWN_TIMEOUT;
            }
        } else if (state == State.CoolDown) {
            timeOut--;
            if (timeOut == 0) {
                state = State.Idle;
            }
        }
    }

    public void attack() {
        if (state == State.Idle) {
            timeOut = ATTACK_TIMEOUT;
            state = State.Attack;
        }
    }

    @Override
    public double getSpeed() {
        return 1;
    }

    public static enum State {
        Idle, Attack, CoolDown
    }
    public static final int ATTACK_TIMEOUT = 60;
    public static final int COOL_DOWN_TIMEOUT = 120;
    private State state = State.Idle;
    private int timeOut = 0;
    public Sailor (Point position) {
        super(position);
        width = 40;
        height = 58;
    }

    @Override
    public void draw() {
        Image image = Images.getImage(state == State.Idle ? Images.SAILOR : Images.SAILOR_HIT, facingDirection);
        image.drawFromTopLeft(position.x, position.y);
    }

    public int getHealthPoint() {
        return healthPoint;
    }

    public void setHealthPoint(int healthPoint) {
        this.healthPoint = healthPoint;
    }

    public int getHealthPointMaximum() {
        return healthPointMaximum;
    }

    public void setHealthPointMaximum(int healthPointMaximum) {
        this.healthPointMaximum = healthPointMaximum;
    }

    public int getDamagePoint() {
        return damagePoint;
    }

    public void setDamagePoint(int damagePoint) {
        this.damagePoint = damagePoint;
    }

    public List<Item> getItems() {
        return items;
    }

    public boolean isAttacking() {
        return state == State.Attack;
    }
}
