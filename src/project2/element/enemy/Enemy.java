package project2.element.enemy;

import bagel.Drawing;
import bagel.Image;
import bagel.util.Colour;
import bagel.util.Point;
import bagel.util.Rectangle;
import project2.element.Element;
import project2.element.MovingElement;
import project2.element.Updatable;
import project2.element.projectile.Projectile;
import project2.util.Direction;
import project2.util.FontUtil;
import project2.util.Images;

import java.util.Random;

public abstract class Enemy extends MovingElement implements Updatable {
    private static Random random = new Random();
    public static enum AttackState {
        ReadyToAttack, CoolDown
    }
    public static enum VisibleState {
        Visible, Invisible
    }
    public static final int INVISIBLE_TIMEOUT = 90;
    public static final int COOL_DOWN_TIMEOUT = 180;
    protected AttackState attackState = AttackState.ReadyToAttack;
    protected VisibleState visibleState = VisibleState.Visible;
    protected int attackTimeOut = 0;
    protected int visibleTimeOut = 0;

    protected int healthPoint = 0;
    protected int damagePoint = 0;
    protected int maximumDamagePoint = 0;
    protected double projectileSpeed = 0;

    public int getRange() {
        return range;
    }

    protected int range;
    @Override
    public void update() {
        if (attackState == AttackState.CoolDown) {
            attackTimeOut--;
            if (attackTimeOut == 0) {
                attackState = AttackState.ReadyToAttack;
            }
        }
        if (visibleState == VisibleState.Invisible) {
            visibleTimeOut--;
            if (visibleTimeOut == 0) {
                visibleState = VisibleState.Visible;
            }
        }
    }

    public void onHarmed(int damagePoint) {
        if (visibleState == VisibleState.Invisible) {
            return;
        }
        healthPoint -= damagePoint;
        visibleState = VisibleState.Invisible;
        visibleTimeOut = INVISIBLE_TIMEOUT;
    }

    public void onAttacked() {
        if (attackState == AttackState.CoolDown) {
            return;
        }
        attackState = AttackState.CoolDown;
        attackTimeOut = COOL_DOWN_TIMEOUT;
    }

    @Override
    public double getSpeed() {
        return random.nextDouble() / 2 + 0.2;
    }

    public Projectile createProjectile(Point position) {
        double speedValue = projectileSpeed;
        Point selfPosition = new Point(this.position.x + getWidth() / 2, this.position.y + getHeight() / 2);
        double distance = Math.sqrt((position.x - selfPosition.x) * (position.x - selfPosition.x) + (position.y - selfPosition.y) * (position.y - selfPosition.y));
        double speedX = speedValue * (position.x - selfPosition.x) / distance;
        double speedY = speedValue * (position.y - selfPosition.y) / distance;
        return new Projectile(this.position, 0, new Point(speedX, speedY), damagePoint);
    }

    @Override
    public void draw() {
        /*int range = getRange();
        Point centerPosition = new Point(getPosition().x + getWidth() / 2, getPosition().y + getHeight() / 2);
        Point leftUpPosition = new Point(centerPosition.x - range / 2, centerPosition.y - range / 2);
        Drawing.drawRectangle(leftUpPosition, range, range, Colour.WHITE);*/

        Image image = Images.getImage(visibleState == VisibleState.Visible ? key : key + "Hit", facingDirection);
        image.drawFromTopLeft(position.x, position.y);
        FontUtil.drawEnemyHealthPoint(getPosition(), healthPoint * 100 / maximumDamagePoint);
    }

    public int getHealthPoint() {
        return healthPoint;
    }

    public Enemy(Point position) {
        super(position);
        width = 40;
        height = 58;
        direction = facingDirection = Direction.Right;
    }

    public Enemy() {
        this(new Point(0, 0));
    }

    public boolean isCoolingDown() {
        return attackState == AttackState.CoolDown;
    }
}
