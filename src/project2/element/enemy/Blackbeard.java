package project2.element.enemy;


import bagel.util.Point;
import project2.element.projectile.Projectile;
import project2.util.Images;

public class Blackbeard extends Enemy {

    public Blackbeard(Point location) {
        super(location);
        key = Images.BLACKBEARD;
        healthPoint = 90;
        maximumDamagePoint = 90;
        range = 200;
        damagePoint = 20;
        projectileSpeed = 0.8;
    }
}
