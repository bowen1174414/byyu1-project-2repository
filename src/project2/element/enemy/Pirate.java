package project2.element.enemy;

import bagel.util.Point;
import project2.element.projectile.Projectile;
import project2.util.Images;

public class Pirate extends Enemy {
    public Pirate(Point position) {
        super(position);
        key = Images.PIRATE;
        healthPoint = 45;
        maximumDamagePoint = 45;
        range = 100;
        damagePoint = 10;
        projectileSpeed = 0.4;
    }
}
