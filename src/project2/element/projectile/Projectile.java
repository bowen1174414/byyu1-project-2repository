package project2.element.projectile;

import bagel.DrawOptions;
import bagel.util.Point;
import project2.element.Element;
import project2.element.MovingElement;
import project2.element.Updatable;
import project2.util.Images;

public class Projectile extends Element implements Updatable {
    private double angle;
    private Point speed;
    private int damagePoint;

    @Override
    public void update() {
        position = new Point(position.x + speed.x, position.y + speed.y);
    }

    public Projectile(Point position, double angle, Point speed, int damagePoint) {
        super(position);
        this.angle = angle;
        this.speed = speed;
        this.damagePoint = damagePoint;
    }

    public double getAngle() {
        return angle;
    }

    public Point getSpeed() {
        return speed;
    }

    public int getDamagePoint() {
        return damagePoint;
    }

    @Override
    public void draw() {
        DrawOptions options = new DrawOptions();
        options.setRotation(Math.atan2(speed.y, speed.x));
        if (damagePoint == 10) {
            Images.PirateProjectile.drawFromTopLeft(position.x, position.y, options);
        } else {
            Images.BlackbeardProjectile.drawFromTopLeft(position.x, position.y, options);
        }
    }
}
