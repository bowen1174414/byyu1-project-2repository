package project2;

import bagel.*;
import project2.state.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Skeleton Code for SWEN20003 Project 2, Semester 1, 2022
 *
 * Please fill your name below
 * @author
 */
public class ShadowPirate extends AbstractGame implements StateHolder {
    private final static int WINDOW_WIDTH = 1024;
    private final static int WINDOW_HEIGHT = 768;
    private final static String GAME_TITLE = "project2.ShadowPirate";
    private final Image BACKGROUND_IMAGE = new Image("res/background0.png");

    public ShadowPirate() {
        super(WINDOW_WIDTH, WINDOW_HEIGHT, GAME_TITLE);
    }

    /**
     * The entry point for the program.
     */
    public static void main(String[] args) {
        ShadowPirate game = new ShadowPirate();
        game.initStates();
        game.run();
    }

    /**
     * Method used to read file and create objects
     */
    private void readCSV(String fileName){

    }

    /**
     * Performs a project2.state update.
     * allows the game to exit when the escape key is pressed.
     */
    @Override
    public void update(Input input) {
        //BACKGROUND_IMAGE.draw(Window.getWidth()/2.0, Window.getHeight()/2.0);



        if (input.wasPressed(Keys.ESCAPE)){
            Window.close();
        }
        for (Keys key : handledKeys) {
            if (input.isDown(key)) {
                currentState.handleKeyPress(key);
            }
        }
        currentState.update();
        currentState.draw();

    }


    private Map<String, State> stateMap = new HashMap<>();
    private State currentState = null;

    public void initStates() {
        stateMap.put(State.STATE_LEVEL1_BEGIN, new LevelBeginState(this, List.of(
                "PRESS SPACE TO START",
                "PRESS S TO ATTACK",
                "USE ARROW KEYS TO FIND LADDER"
        ), 0));
        stateMap.put(State.STATE_LEVEL2_BEGIN, new LevelBeginState(this, List.of(
                "PRESS SPACE TO START",
                "PRESS S TO ATTACK",
                "FIND THE TREASURE"), 1));
        stateMap.put(State.STATE_LEVEL1, new GamingState(this, "res/level0.csv", 0));
        stateMap.put(State.STATE_LEVEL2, new GamingState(this, "res/level1.csv", 1));
        stateMap.put(State.STATE_GAME_WIN, new GameWinState(this));
        stateMap.put(State.STATE_GAME_OVER, new GameOverState(this));
        currentState = stateMap.get(State.STATE_LEVEL1_BEGIN);
    }

    @Override
    public void changeState(String key, Object obj) {
        currentState = stateMap.get(key);
        currentState.onEntering(obj);
    }

    private Keys[] handledKeys = new Keys[]{Keys.LEFT, Keys.RIGHT, Keys.UP, Keys.DOWN, Keys.SPACE, Keys.S};
}
