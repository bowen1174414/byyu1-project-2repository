package project2.state;

import bagel.Drawing;
import bagel.Keys;
import bagel.Window;
import bagel.util.Colour;
import project2.util.FontUtil;

public class GameWinState extends State {
    public GameWinState(StateHolder stateHolder) {
        super(stateHolder);
    }

    @Override
    public void draw() {
        Drawing.drawRectangle(0, 0, Window.getWidth(), Window.getHeight(), Colour.WHITE);
        String text = "CONGRATULATIONS!";
        double x = (Window.getWidth() - FontUtil.getTextWidth(text)) / 2 ;
        FontUtil.drawString(text, x, 402, Colour.BLACK);
    }

    @Override
    public void handleKeyPress(Keys key) {

    }
}
