package project2.state;

import bagel.Keys;

public abstract class State {
    public State(StateHolder stateHolder) {
        this.stateHolder = stateHolder;
    }

    public abstract void draw();
    public abstract void handleKeyPress(Keys key);
    public void update() {}

    protected StateHolder stateHolder;

    public void onEntering(Object obj) {}

    public static final String STATE_LEVEL1_BEGIN = "Level1Begin";
    public static final String STATE_LEVEL2_BEGIN = "Level2Begin";
    public static final String STATE_LEVEL1 = "Level1";
    public static final String STATE_LEVEL2 = "Level2";
    public static final String STATE_GAME_WIN = "GameWin";
    public static final String STATE_GAME_OVER = "GameOver";
}
