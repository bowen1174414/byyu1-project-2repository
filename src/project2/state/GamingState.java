package project2.state;

import bagel.Keys;
import bagel.util.Point;
import project2.GameMap;
import project2.element.Sailor;
import project2.util.Direction;
import project2.util.GameMapLoader;

public class GamingState extends State {
    private GameMap gameMap = null;
    public GamingState(StateHolder stateHolder, String fileName, int level) {
        super(stateHolder);
        gameMap = GameMapLoader.createGameMap(fileName, level);
    }

    @Override
    public void draw() {
        gameMap.draw();
    }

    @Override
    public void handleKeyPress(Keys key) {
        Direction direction = Direction.None;
        switch (key) {
            case LEFT: direction = Direction.Left; break;
            case RIGHT: direction = Direction.Right; break;
            case UP: direction = Direction.Up; break;
            case DOWN: direction = Direction.Down; break;
        }
        if (direction != Direction.None) {
            gameMap.moveSailor(direction);
        }
        if (key == Keys.S) {
            gameMap.sailorAttack();
        }
    }

    @Override
    public void update() {
        gameMap.update();
        if (gameMap.checkIfSailorDead()) {
            stateHolder.changeState(State.STATE_GAME_OVER, null);
        } else if (gameMap.checkIfSailorOnLadder()) {
            stateHolder.changeState(State.STATE_LEVEL2, null);
        } else if (gameMap.checkIfWin()) {
            stateHolder.changeState(State.STATE_GAME_WIN, null);
        }
    }

    @Override
    public void onEntering(Object obj) {
        if (!(obj instanceof Sailor)) {
            return;
        }
    }

}
