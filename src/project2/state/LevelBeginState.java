package project2.state;

import bagel.Drawing;
import bagel.Keys;
import bagel.Window;
import bagel.util.Colour;
import project2.util.FontUtil;

import java.util.List;

public class LevelBeginState extends State {
    private final List<String> texts;
    private Object obj;
    private final int nextLevel;
    public LevelBeginState(StateHolder stateHolder, List<String> texts, int nextLevel) {
        super(stateHolder);
        this.texts = texts;
        this.nextLevel = nextLevel;
    }
    private int timeOut = 180;

    @Override
    public void update() {
        if (timeOut > 0) {
            timeOut--;
        }
    }

    @Override
    public void draw() {
        Drawing.drawRectangle(0, 0, Window.getWidth(), Window.getHeight(), Colour.WHITE);
        if (nextLevel == 1 && timeOut > 0) {
            int y = 402;
            String text = "LEVEL COMPLETE!";
            double x = (Window.getWidth() - FontUtil.getTextWidth(text)) / 2 ;
            FontUtil.drawString(text, x, y, Colour.BLACK);
        } else {
            int y = 402;
            for (String text : texts) {
                double x = (Window.getWidth() - FontUtil.getTextWidth(text)) / 2 ;
                FontUtil.drawString(text, x, y, Colour.BLACK);
                y += 58;
            }
        }
    }

    @Override
    public void handleKeyPress(Keys key) {
        if (key == Keys.SPACE) {
            if (nextLevel == 0) {
                stateHolder.changeState(State.STATE_LEVEL1, null);
            } else {
                stateHolder.changeState(State.STATE_LEVEL2, null);
            }
        }
    }

    @Override
    public void onEntering(Object obj) {
        this.obj = obj;
    }
}
