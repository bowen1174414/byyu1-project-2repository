package project2.state;

public interface StateHolder {
    void changeState(String key, Object obj);
}
